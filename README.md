[![coverage report](https://gitlab.com/al-v-in_proj/my-media/frontend/badges/master/coverage.svg)](https://gitlab.com/al-v-in_proj/my-media/frontend/-/commits/master) 
[![pipeline status](https://gitlab.com/al-v-in_proj/my-media/frontend/badges/master/pipeline.svg)](https://gitlab.com/al-v-in_proj/my-media/frontend/-/commits/master)

# My Media - Frontend

Angular front end web application for My Media app

## Local Development

Run `npm install` or `npm ci` to install dependencies

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Docker

Run `docker build --tag my-media-frontend .` to build image

Run `docker run --volume ${PWD}:/app --publish 127.0.0.1:8080:4200 my-media-frontend` to serve application at `http://localhost:8080/`. `--volume` (`-v`) is optional, but allows live reloading. Remove `127.0.0.1` from `--publish` (`-p`) if you require network access.
