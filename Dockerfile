FROM trion/ng-cli:10.0.2

WORKDIR /app

COPY . /app

RUN npm ci

CMD ["ng", "serve", "--host", "0.0.0.0"]
