import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaListComponent } from './media-list.component';

describe('MediaListComponent', () => {
  let component: MediaListComponent;
  let fixture: ComponentFixture<MediaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('List of my media:');
  });

  it('should render list of media', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.media-list').textContent).toContain('Lord Of The Rings');
    expect(compiled.querySelector('.media-list').textContent).toContain('The Last Of Us');
  });
});
