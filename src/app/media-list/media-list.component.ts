import { Component, OnInit } from '@angular/core';
import { Media } from '../models/media';

@Component({
  selector: 'app-media-list',
  templateUrl: './media-list.component.html',
  styleUrls: ['./media-list.component.scss']
})
export class MediaListComponent implements OnInit {
  media: Media[] = [
    {
      id: 1,
      title: 'Lord Of The Rings'
    },
    {
      id: 2,
      title: '1984'
    },
    {
      id: 3,
      title: 'The Last Of Us'
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
